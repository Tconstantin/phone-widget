<?php
declare(strict_types = 1);
namespace tc\phone;

use yii\bootstrap\Html;
use yii\bootstrap\InputWidget;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * @property \yii\web\View              $view
 * @property \yii\bootstrap\ActiveField $field
 */
class PhoneInput extends InputWidget
{
    const PLUGIN_NAME = 'inputphone';

    public $view = 'index';

    /** @var string HTML tag type of the widget input ("tel" by default) */
    public $input_type = 'tel';

    public $group_options = ['class' => 'input-group input-phone'];

    /** @var array Default widget options of the HTML tag */
    protected static $default_options = [
        'class' => ['form-control'],
        'type'  => 'tel',
    ];

    /** @var string Default template */
    public $template = '';

    /** @var array Options of the JS-widget */
    public $jsOptions = [];

    public function init()
    {
        parent::init();
        $this->options['id'] = ArrayHelper::getValue($this->options, 'id', $this->getId());

        $this->options['class'] =
            implode(' ', ArrayHelper::merge(self::$default_options['class'], explode(' ', ArrayHelper::getValue($this->options, 'class', ''))));

        $this->options['aria-describedby'] = ArrayHelper::getValue($this->options, 'aria-describedby', $this->getId(), $this->options['id']);

        $name = $this->options['name'] ?? Html::getInputName($this->model, $this->attribute);
        $value = $this->options['value'] ?? Html::getAttributeValue($this->model, $this->attribute);

        if ($this->template === '') {
            $this->template = implode("\n", [
                Html::beginTag('div', $this->group_options),
                Html::tag('div', '', ['class' => 'input-group-addon flags-container']),
                isset($this->field->parts['{input}']) ? $this->field->parts['{input}'] : Html::input($this->input_type, $name, $value, $this->options),
                Html::endTag('div'),
            ]);
        }
    }

    /**
     * @return string
     */
    public function run():string
    {
        $this->registerClientScript();
        return $this->template;
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        PhoneInputAsset::register($this->view);
        $id = $this->options['id'];
        $jsOptions = $this->jsOptions ? Json::encode($this->jsOptions) : "";
        $this->view->registerJs('jQuery("#' . $id . '").' . self::PLUGIN_NAME . '(' . $jsOptions . ');');
    }
}