<?php
declare(strict_types = 1);
namespace tc\phone;


use FrontApp\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;


/**
 * Asset Bundle of the phone input widget. Registers required CSS and JS files.
 *
 * @package phoneInput
 */
class PhoneInputAsset extends AssetBundle
{

    /**
     * PhoneInputAsset constructor.
     */
    public function __construct()
    {
        parent::__construct([
            'sourcePath' => '@vendor/tc/phone/src/assets',
            'js'         => [
                'js/utils.js',
                'js/phone.js',
            ],
            'css'        => [
                'css/phone.css',
            ],
            'depends'    => [
                AppAsset::class,
                //JqueryAsset::class,
            ],
        ]);
    }

}
